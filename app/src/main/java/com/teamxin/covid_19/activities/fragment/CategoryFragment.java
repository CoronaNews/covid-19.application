package com.teamxin.covid_19.activities.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.teamxin.covid_19.R;
import com.teamxin.covid_19.activities.DisplaylistActivity;
import com.teamxin.covid_19.models.DataList;
import com.teamxin.covid_19.utils.UrlApiCovid;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.CALL_PHONE;

public class CategoryFragment extends Fragment implements View.OnClickListener {
    private ProgressDialog progressDialog;
    private CardView cvNews, cvVideos, cvKhuyencao, cvDieucanbiet, cvHotro, cvChidao;
    private View view;
    private LinearLayout llcall;

    public CategoryFragment() {
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_category, container, false);
        Connectview();
        Listener();
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cvNews:
                Request(UrlApiCovid.getNews());
                break;
            case R.id.cvVideos:
                Request(UrlApiCovid.getVideos());
                break;
            case R.id.cvKhuyencao:
                Request(UrlApiCovid.getKhuyenCao());
                break;
            case R.id.cvDieucanbiet:
                Request(UrlApiCovid.getDieuCanBiet());
                break;
            case R.id.cvHotro:
                Request(UrlApiCovid.getHoTroTrongNganh());
                break;
            case R.id.cvChidao:
                Request(UrlApiCovid.getChiDaoDieuHanh());
                break;
            case R.id.linearLayoutCall:
                Makecall();
                break;
        }

    }

    private void Makecall() {
        if (ContextCompat.checkSelfPermission(getContext(), CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage("Bạn muốn gọi cho bộ y tế ?")
                    .setCancelable(false)
                    .setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            startActivity(new Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:19009095")));
                        }
                    })
                    .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.setTitle("Thông báo");
            alert.show();
        } else
            requestPermissions(new String[]{CALL_PHONE}, 1);
    }

    private void Request(final String url) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Đang cập nhật");
        progressDialog.show();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url + "1", null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressDialog.dismiss();
                        ArrayList<DataList> array = new Gson().fromJson(response.toString(), new TypeToken<List<DataList>>() {
                        }.getType());
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("array", array);
                        Intent intent = new Intent(getActivity(), DisplaylistActivity.class);
                        intent.putExtras(bundle);
                        intent.putExtra("url", url);
                        startActivity(intent);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), "Có lỗi xảy ra.Vui lòng thử lại sau", Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(jsonArrayRequest);
    }

    private void Listener() {
        cvNews.setOnClickListener(this);
        cvVideos.setOnClickListener(this);
        cvKhuyencao.setOnClickListener(this);
        cvDieucanbiet.setOnClickListener(this);
        cvHotro.setOnClickListener(this);
        cvChidao.setOnClickListener(this);
        llcall.setOnClickListener(this);
    }

    private void Connectview() {
        cvNews = view.findViewById(R.id.cvNews);
        cvVideos = view.findViewById(R.id.cvVideos);
        cvKhuyencao = view.findViewById(R.id.cvKhuyencao);
        cvDieucanbiet = view.findViewById(R.id.cvDieucanbiet);
        cvHotro = view.findViewById(R.id.cvHotro);
        cvChidao = view.findViewById(R.id.cvChidao);
        llcall = view.findViewById(R.id.linearLayoutCall);
    }
}
