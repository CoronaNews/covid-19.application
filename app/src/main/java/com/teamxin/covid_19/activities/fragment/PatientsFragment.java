package com.teamxin.covid_19.activities.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonArrayRequest;
import com.teamxin.covid_19.R;
import com.teamxin.covid_19.adapters.AdapterPatients;
import com.teamxin.covid_19.utils.UrlApiCovid;

import org.json.JSONArray;

public final class PatientsFragment extends Fragment {
    private RecyclerView recyclerView;

    public PatientsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_patients, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Cache cache = new DiskBasedCache(getContext().getCacheDir(), 1024 * 1024);

        Network network = new BasicNetwork(new HurlStack());
        RequestQueue requestQueue = new RequestQueue(cache, network);

        requestQueue.start();
        requestQueue.add(jsonArrayRequest);
        recyclerView = view.findViewById(R.id.rvPatients);
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.bottomup);
        recyclerView.startAnimation(bottomUp);
    }


    private JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, UrlApiCovid.getPatients(), null,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    AdapterPatients adapterPatients = new AdapterPatients(response);

                    LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(adapterPatients);
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getContext(), "Có lỗi xảy ra.Vui lòng thử lại sau", Toast.LENGTH_SHORT).show();
                }
            });
}
