package com.teamxin.covid_19.activities.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.teamxin.covid_19.R;
import com.teamxin.covid_19.adapters.AdapterProvinces;
import com.teamxin.covid_19.utils.UrlApiCovid;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public final class HomeFragment extends Fragment {
    private ImageView bg;
    private ProgressDialog progressDialog;
    private TextView tvconfirmedVN, tvtreatingVN, tvrecoveredVN, tvdeathsVN, tvconfirmedGlobal, tvtreatingGlobal, tvrecoveredGlobal, tvdeathsGlobal;
    private RecyclerView recyclerView;
    private View view;
    private int check = 3;

    public HomeFragment() {
    }

    public HomeFragment(ImageView bg) {
        this.bg = bg;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        ConnectView();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.rVDienbien);
        SetView();
    }


    private void ConnectView() {
        tvconfirmedVN = view.findViewById(R.id.tvconfirmedVN);
        tvtreatingVN = view.findViewById(R.id.tvtreatingVN);
        tvrecoveredVN = view.findViewById(R.id.tvrecoveredVN);
        tvdeathsVN = view.findViewById(R.id.tvdeathsVN);

        tvconfirmedGlobal = view.findViewById(R.id.tvconfirmedGlobal);
        tvtreatingGlobal = view.findViewById(R.id.tvtreatingGlobal);
        tvrecoveredGlobal = view.findViewById(R.id.tvrecoveredGlobal);
        tvdeathsGlobal = view.findViewById(R.id.tvdeathsGlobal);
    }

    private void SetView() {
        Cache cache = new DiskBasedCache(getContext().getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());
        RequestQueue requestQueue = new RequestQueue(cache, network);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Đang cập nhật");
        progressDialog.show();

        requestQueue.start();
        requestQueue.add(VietNamObject);
        requestQueue.add(GlobalObject);
        requestQueue.add(ProvincialArrayObject);
        CheckFinish();
    }


    private void SetAnimator() {
        ImageView imgVN = view.findViewById(R.id.imgVN);
        ImageView imgGlobal = view.findViewById(R.id.imgGlobal);
        CardView cvVN = view.findViewById(R.id.cvVN);
        CardView cvTG = view.findViewById(R.id.cvTG);
        CardView cvDienbien = view.findViewById(R.id.cvDienbien);
        TextView tvVN = view.findViewById(R.id.tvVietNam);
        TextView tvTG = view.findViewById(R.id.tvGlobal);
        TextView tvDienbien = view.findViewById(R.id.tvDienbien);
        TextView tvnguon = view.findViewById(R.id.tvnguon);

        //animation
        bg.animate().translationY(-2300).setDuration(600);
        Animation bottomUp = AnimationUtils.loadAnimation(getContext(), R.anim.bottomup);
        cvVN.startAnimation(bottomUp);
        cvTG.startAnimation(bottomUp);
        cvDienbien.startAnimation(bottomUp);
        tvVN.startAnimation(bottomUp);
        tvTG.startAnimation(bottomUp);
        tvnguon.startAnimation(bottomUp);
        tvDienbien.startAnimation(bottomUp);
        imgVN.startAnimation(bottomUp);
        imgGlobal.startAnimation(bottomUp);
    }


    private void CheckFinish() {
        new Thread(new Runnable() {
            public void run() {
                while (true)
                    if (check == 0) {
                        SetAnimator();
                        progressDialog.dismiss();
                        break;
                    }
            }
        }).start();
    }

    private JsonObjectRequest VietNamObject = new JsonObjectRequest(Request.Method.GET, UrlApiCovid.getVietNam(), null,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        tvconfirmedVN.setText(response.getString("confirmed"));
                        tvtreatingVN.setText(response.getString("treating"));
                        tvrecoveredVN.setText(response.getString("recovered"));
                        tvdeathsVN.setText(response.getString("deaths"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    check -= 1;
                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            check -= 1;
            Toast.makeText(getContext(), "Có lỗi xảy ra.Vui lòng thử lại sau", Toast.LENGTH_SHORT).show();
        }
    });

    private JsonObjectRequest GlobalObject = new JsonObjectRequest(Request.Method.GET, UrlApiCovid.getGlobal(), null,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        tvconfirmedGlobal.setText(response.getString("confirmed"));
                        tvtreatingGlobal.setText(response.getString("treating"));
                        tvrecoveredGlobal.setText(response.getString("recovered"));
                        tvdeathsGlobal.setText(response.getString("deaths"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    check -= 1;
                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            check -= 1;
            Toast.makeText(getContext(), "Có lỗi xảy ra.Vui lòng thử lại sau", Toast.LENGTH_SHORT).show();
        }
    });

    private JsonArrayRequest ProvincialArrayObject = new JsonArrayRequest(Request.Method.GET, UrlApiCovid.getProvincial(), null,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    AdapterProvinces adapterProvinces = new AdapterProvinces(response);

                    LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(adapterProvinces);
                    check -= 1;
                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            check -= 1;
            Toast.makeText(getContext(), "Có lỗi xảy ra.Vui lòng thử lại sau", Toast.LENGTH_SHORT).show();
        }
    });
}