package com.teamxin.covid_19.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;

import com.teamxin.covid_19.R;
import com.teamxin.covid_19.activities.fragment.HomeFragment;
import com.teamxin.covid_19.activities.fragment.CategoryFragment;
import com.teamxin.covid_19.activities.fragment.TimelineFragment;
import com.teamxin.covid_19.activities.fragment.PatientsFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {
    private Fragment home, search, patients, timeline, present;

    FragmentManager fragmentManager = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (isOnline()) {
            BottomNavigationView bottomNavigationView = findViewById(R.id.bottomnavigation);
            bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

            addFragment();
        } else
            DialogError();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.nav_home:
                    loadFragment(home);
                    present = home;
                    return true;
                case R.id.nav_total:
                    loadFragment(patients);
                    present = patients;
                    return true;
                case R.id.nav_list:
                    loadFragment(search);
                    present = search;
                    return true;
                case R.id.nav_timeline:
                    loadFragment(timeline);
                    present = timeline;
                    return true;
            }
            return false;
        }
    };

    private void addFragment() {
        ImageView bg = findViewById(R.id.bg);
        home = new HomeFragment(bg);
        patients = new PatientsFragment();
        timeline = new TimelineFragment();
        search = new CategoryFragment();
        fragmentManager.beginTransaction().add(R.id.fragment, patients).hide(patients).commit();
        fragmentManager.beginTransaction().add(R.id.fragment, search).hide(search).commit();
        fragmentManager.beginTransaction().add(R.id.fragment, timeline).hide(timeline).commit();
        fragmentManager.beginTransaction().add(R.id.fragment, home).commit();
        present = home;
    }

    private void loadFragment(Fragment fragment) {
        fragmentManager.beginTransaction().hide(present).show(fragment).commit();
    }

    private Boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    private void DialogError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Hãy kiểm tra lại internet và thử lại")
                .setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.setTitle("Thông báo");
        alert.show();
    }
}
