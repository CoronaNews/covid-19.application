package com.teamxin.covid_19.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.teamxin.covid_19.R;
import com.teamxin.covid_19.adapters.AdapterDisplaylist;
import com.teamxin.covid_19.models.DataList;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class DisplaylistActivity extends AppCompatActivity {
    boolean isLoading = false, isEnd = false;
    RecyclerView recyclerView;
    AdapterDisplaylist adapterDisplaylist;
    LinearLayoutManager layoutManager;
    ArrayList<DataList> dataListArrayList;
    int next = 2;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_displaylist);


        Bundle args = getIntent().getExtras();
        url = getIntent().getStringExtra("url");
        dataListArrayList = (ArrayList<DataList>) args.getSerializable("array");
        adapterDisplaylist = new AdapterDisplaylist(dataListArrayList, this);

        recyclerView = findViewById(R.id.rvView);
        recyclerView.setAdapter(adapterDisplaylist);

        layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        initScrollListener();
    }

    private void initScrollListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!isLoading && !isEnd)
                    if (layoutManager != null && layoutManager.findLastVisibleItemPosition() == dataListArrayList.size() - 1) {
                        recyclerView.post(new Runnable() {
                            @Override
                            public void run() {
                                loadMore();
                            }
                        });
                        isLoading = true;
                    }
            }
        });
    }

    private void loadMore() {
        dataListArrayList.add(null);
        adapterDisplaylist.notifyItemInserted(dataListArrayList.size() - 1);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url + next++, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        dataListArrayList.remove(dataListArrayList.size() - 1);
                        adapterDisplaylist.notifyItemRemoved(dataListArrayList.size());
                        ArrayList<DataList> arr = new Gson().fromJson(response.toString(), new TypeToken<List<DataList>>() {
                        }.getType());
                        if (arr.size() != 0) {
                            int currentPosition = dataListArrayList.size();
                            dataListArrayList.addAll(arr);
                            adapterDisplaylist.notifyItemRangeInserted(currentPosition, arr.size());
                            isLoading = false;
                        } else isEnd = true;
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dataListArrayList.remove(dataListArrayList.size() - 1);
                adapterDisplaylist.notifyItemRemoved(dataListArrayList.size());
                Log.e("Er", error.getMessage());
                Toast.makeText(getApplicationContext(), "Có lỗi xảy ra.Hãy thử lại", Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.start();
        requestQueue.add(jsonArrayRequest);
    }
}
