package com.teamxin.covid_19.models;


import java.io.Serializable;

public class DataList implements Serializable {
    private String title = "";
    private String url = "";
    private String src = "";
    private String description = "";
    private String poster = "";


    public String getPoster() {
        return poster;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getSrc() {
        return src;
    }

    public String getDescription() {
        return description;
    }

}
