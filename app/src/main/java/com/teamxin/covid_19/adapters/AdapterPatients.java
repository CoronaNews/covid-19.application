package com.teamxin.covid_19.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.teamxin.covid_19.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdapterPatients extends RecyclerView.Adapter<AdapterPatients.DataViewHolder> {

    private Context context;
    private JSONArray jsonArray;

    public AdapterPatients(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
    }

    @NonNull
    @Override
    public AdapterPatients.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        context = parent.getContext();
        View view = inflater.inflate(R.layout.itempatients_recyclerview, parent, false);
        return new DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        try {
            JSONObject jsonObject = jsonArray.getJSONObject(position);
            holder.tvpatientCodeRV.setText(jsonObject.getString("patientCode"));
            holder.tvagedRV.setText(jsonObject.getString("age"));
            holder.tvgenderRV.setText(jsonObject.getString("gender"));
            holder.tvaddressRV.setText(jsonObject.getString("address"));
            holder.ivstatusRV.setBackgroundResource((jsonObject.getString("status").equals("Khỏi") ? R.drawable.ok : (jsonObject.getString("status").equals("Đang điều trị") ? R.drawable.care : R.drawable.dead)));
            holder.tvnationalityRV.setText(jsonObject.getString("nationality"));

            holder.constraintLayout.startAnimation(AnimationUtils.loadAnimation(context, R.anim.slide_right));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    class DataViewHolder extends RecyclerView.ViewHolder {
        TextView tvpatientCodeRV, tvagedRV, tvgenderRV, tvaddressRV, tvnationalityRV;
        ImageView ivstatusRV;
        ConstraintLayout constraintLayout;

        DataViewHolder(@NonNull View itemView) {
            super(itemView);
            tvpatientCodeRV = itemView.findViewById(R.id.tvpatientCodeRV);
            tvagedRV = itemView.findViewById(R.id.tvagedRV);
            tvgenderRV = itemView.findViewById(R.id.tvgenderRV);
            tvaddressRV = itemView.findViewById(R.id.tvaddressRV);
            tvnationalityRV = itemView.findViewById(R.id.tvnationalityRV);
            ivstatusRV = itemView.findViewById(R.id.ivstatusRV);
            constraintLayout = itemView.findViewById(R.id.containerPatitents);
        }
    }
}
