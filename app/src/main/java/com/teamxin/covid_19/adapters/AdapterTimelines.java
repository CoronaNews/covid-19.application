package com.teamxin.covid_19.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.teamxin.covid_19.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdapterTimelines extends RecyclerView.Adapter<AdapterTimelines.DataViewHolder> {

    private JSONArray jsonArray;

    public AdapterTimelines(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
    }

    @NonNull
    @Override
    public AdapterTimelines.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.itemtimelines_recyclerview, parent, false);
        return new DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        try {
            JSONObject jsonObject = jsonArray.getJSONObject(position);
            holder.tvtimeStampRV.setText(jsonObject.getString("timeStamp"));
            holder.tvcontentRV.setText(jsonObject.getString("content"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    static class DataViewHolder extends RecyclerView.ViewHolder {
        TextView tvtimeStampRV, tvcontentRV;

        DataViewHolder(@NonNull View itemView) {
            super(itemView);
            tvtimeStampRV = itemView.findViewById(R.id.tvtimeStampRV);
            tvcontentRV = itemView.findViewById(R.id.tvcontentRV);
        }
    }
}
