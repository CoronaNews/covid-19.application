package com.teamxin.covid_19.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.teamxin.covid_19.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdapterProvinces extends RecyclerView.Adapter<AdapterProvinces.DataViewHolder> {

    private Context context;
    private JSONArray jsonArray;

    public AdapterProvinces(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
    }

    @NonNull
    @Override
    public AdapterProvinces.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        context = parent.getContext();
        View view = inflater.inflate(R.layout.itemprovinces_recyclerview, parent, false);
        return new DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        try {
            JSONObject jsonObject = jsonArray.getJSONObject(position);
            holder.tvtenRV.setText(jsonObject.getString("name"));
            holder.tvconfirmedRV.setText(jsonObject.getString("confirmed"));
            holder.tvtreatingRV.setText(jsonObject.getString("treating"));
            holder.tvrecoveredRV.setText(jsonObject.getString("recovered"));
            holder.tvdeathsRV.setText(jsonObject.getString("deaths"));

            holder.constraintLayout.startAnimation(AnimationUtils.loadAnimation(context, R.anim.slide_right));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    class DataViewHolder extends RecyclerView.ViewHolder {
        TextView tvtenRV, tvconfirmedRV, tvtreatingRV, tvrecoveredRV, tvdeathsRV;
        ConstraintLayout constraintLayout;

        DataViewHolder(@NonNull View itemView) {
            super(itemView);
            tvtenRV = itemView.findViewById(R.id.tvtenRV);
            tvconfirmedRV = itemView.findViewById(R.id.tvconfirmedRV);
            tvtreatingRV = itemView.findViewById(R.id.tvtreatingRV);
            tvrecoveredRV = itemView.findViewById(R.id.tvrecoveredRV);
            tvdeathsRV = itemView.findViewById(R.id.tvdeathsRV);
            constraintLayout = itemView.findViewById(R.id.containerProvinces);
        }
    }
}
