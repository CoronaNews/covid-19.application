package com.teamxin.covid_19.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.teamxin.covid_19.networks.HttpsTrustManager;
import com.teamxin.covid_19.R;
import com.teamxin.covid_19.activities.WebDisplayActivity;
import com.teamxin.covid_19.models.DataList;


import java.util.ArrayList;

interface ItemClickListener {
    void onClick(View view, int position, boolean isLongClick);
}

class ViewHolderDisplaylist extends RecyclerView.ViewHolder implements View.OnClickListener {
    ProgressBar progressBar;
    ItemClickListener itemClickListener;
    TextView tvtitle, tvdescription;
    NetworkImageView img;

    ViewHolderDisplaylist(@NonNull View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
    }

    void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v, getAdapterPosition(), false);
    }
}

class ItemViewHolder extends ViewHolderDisplaylist {
    ItemViewHolder(@NonNull View itemView) {
        super(itemView);
        tvtitle = itemView.findViewById(R.id.tvtitle);
        tvdescription = itemView.findViewById(R.id.tvdescription);
        img = itemView.findViewById(R.id.imgView);
    }
}

class LoadingViewHolder extends ViewHolderDisplaylist {
    LoadingViewHolder(@NonNull View itemView) {
        super(itemView);
        progressBar = itemView.findViewById(R.id.progressBar);
    }
}

public class AdapterDisplaylist extends RecyclerView.Adapter<ViewHolderDisplaylist> {
    private ArrayList<DataList> arrayList;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private Context context;

    public AdapterDisplaylist(ArrayList<DataList> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolderDisplaylist onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(context).inflate(R.layout.itemview_recyclerview, parent, false);
            return new ItemViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolderDisplaylist holder, int position) {
        if (holder instanceof ItemViewHolder)
            populateItems((ItemViewHolder) holder, position);
        else if (holder instanceof LoadingViewHolder)
            loadingView((LoadingViewHolder) holder);
    }

    private void loadingView(LoadingViewHolder holder) {
        holder.progressBar.setIndeterminate(true);
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return arrayList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    private void populateItems(ItemViewHolder holder, int position) {
        final DataList data = arrayList.get(position);
        setImg(holder, data.getPoster().equals("") ? data.getSrc() : data.getPoster());
        holder.tvtitle.setText(data.getTitle());
        holder.tvdescription.setText(data.getDescription());
        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                Intent intent = new Intent(context, WebDisplayActivity.class);
                intent.putExtra("url", data.getUrl());
                context.startActivity(intent);
            }
        });
    }

    private void setImg(ItemViewHolder holder, String src) {
        HttpsTrustManager.allowAllSSL();
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        ImageLoader imageLoader = new ImageLoader(requestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> cache = new LruCache<>(20);

            @Override
            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url, bitmap);
            }
        });
        holder.img.setImageUrl(src, imageLoader);
        holder.img.setErrorImageResId(R.drawable.error);
    }
}

