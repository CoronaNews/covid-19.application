package com.teamxin.covid_19.utils;

public final class UrlApiCovid {
    private static final String VietNam = "http://150.95.109.21/Covid19/VietNam/";
    private static final String Provincial = "http://150.95.109.21/Covid19/Provinces/";
    private static final String Patients = "http://150.95.109.21/Covid19/Patients/";
    private static final String Global = "http://150.95.109.21/Covid19/Global/";
    private static final String Timelines = "http://150.95.109.21/Covid19/Timelines/1";
    private static final String News = "http://150.95.109.21/Covid19/News/";
    private static final String Videos = "http://150.95.109.21/Covid19/Videos/";
    private static final String KhuyenCao = "http://150.95.109.21/Covid19/Khuyencao/";
    private static final String DieuCanBiet = "http://150.95.109.21/Covid19/DieuCanBiet/";
    private static final String HoTroTrongNganh = "http://150.95.109.21/Covid19/HoTroTrongNganh/";
    private static final String ChiDaoDieuHanh = "http://150.95.109.21/Covid19/ChiDaoDieuHanh/";

    public static String getVideos() {
        return Videos;
    }

    public static String getKhuyenCao() {
        return KhuyenCao;
    }

    public static String getDieuCanBiet() {
        return DieuCanBiet;
    }

    public static String getHoTroTrongNganh() {
        return HoTroTrongNganh;
    }

    public static String getChiDaoDieuHanh() {
        return ChiDaoDieuHanh;
    }

    public static String getNews() {
        return News;
    }

    public static String getTimelines() {
        return Timelines;
    }

    public static String getPatients() {
        return Patients;
    }

    public static String getVietNam() {
        return VietNam;
    }

    public static String getProvincial() {
        return Provincial;
    }

    public static String getGlobal() {
        return Global;
    }

}
